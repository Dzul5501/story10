from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.core import mail
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from .views import story10, checkemail, createuser
from .models import Form
import time
# Create your tests here.
class Story10TestCase(TestCase):
    def test_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_form(self):
        found = resolve('/')
        self.assertEqual(found.func, story10)
    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_check_email(self):
        email = 'asd@asd'
        found = resolve('/checkemail/' + email)
        self.assertEqual(found.func, checkemail)
    def test_add_existing_email(self):
        Form.objects.create(email='asd@asd', name='asd@asd', password='asd@asd')
        response = Client().get('/checkemail'+ 'asd@asd')
        html_response = response.content.decode('utf8')
        self.assertIn("Not Found", html_response)
    def test_send_email(self):
        mail.send_mail(
            'Subject here', 'Here is the message.',
            'from@example.com', ['to@example.com'],
            fail_silently=False,
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Subject here')





