var func = function () {
  if ($('#emailinput').val() != '') {
    url = '/checkemail/' + $('#emailinput').val();
    $("#submitButton").prop('disabled', true);
    $.ajax({
      url: url,
      success: function (response) {
        if (response.substring(4) == "Not available") {
          alert("Email sudah terdaftar, silakan gunakan email yang lain");
          $("#submitButton").prop('disabled', true);
        } else if ($('#emailinput').val() != "" && $('#nameinput').val() != "" && $('#passwordinput').val() != "") {
          $("#submitButton").prop('disabled', false);
        }
      }
    });
  }
  if ($('#emailinput').val() == "" || $('#nameinput').val() == "" || $('#passwordinput').val() == "") {
    $("#submitButton").prop('disabled', true);
  }
}
$(function(){
  $('#story10').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: '/usercreate/',
      data: {
        email: $('#emailinput').val(),
        name: $('#nameinput').val(),
        password: $('#passwordinput').val(),
        csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
      },
      success: function () {
        alert("Pengguna berhasil ditambahkan!")
      }
    });
  }
)
$('#emailinput').keyup(func);
$('#nameinput').keyup(func);
$('#passwordinput').keyup(func);

})
